from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, CreateCategory, CreateAccount
# Create your views here.

@login_required
def Show_Receipts (request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipts_list
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceipt
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner = request.user )
    context = {
        "category_list": category_list
    }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    account_list = Account.objects.filter(owner = request.user)
    context = {
        "account_list": account_list
    }
    return render (request, "receipts/accounts.html", context)

@login_required
def create_category (request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CreateCategory
    context = {
        "form": form
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccount
    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)
